<?php

// Output Hello World to standard output
echo "Hello World";

# Outdated Comment Style
function sampleFunction()
{
}

/**
 * This is a DocBlock.
 */
function associatedFunction()
{
}

// A DocBlock is a piece of inline documentation in your source code that informs you what a class, method or other Structural Element its function is.

/**
 * A summary informing the user what the associated element does.
 *
 * A *description*, that can span multiple lines, to go _in-depth_ into the details of this element
 * and to provide some background information or textual references.
 *
 * @param string $myArgument With a *description* of this argument, these may also
 *    span multiple lines.
 *
 * @return void
 */
 function myFunction($myArgument)
 {
 }


// Source:
// https://phpdoc.org/docs/latest/getting-started/your-first-set-of-documentation.html










