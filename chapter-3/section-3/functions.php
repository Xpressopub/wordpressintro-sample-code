<?php

/**
 * A summary informing the user what the associated element does.
 *
 * A *description*, that can span multiple lines, to go _in-depth_ into the details of this element
 * and to provide some background information or textual references.
 *
 * @param string $myArgument With a *description* of this argument, these may also
 *    span multiple lines.
 *
 * @return void
 */
 function getPi($p){
    $sum = 0;
    for($n = 0; $n < $p; $n++){
        $mult = ($n%2 === 0) ? 1 : -1; // -1^n
        $sum += $mult * (1 / (2*$n+1));
    }
    return $sum * 4; // to get pi
 }
 
 
 $p = 10; // seed value
 
 echo "\nActual value of Pi\n";
 echo "3.1415926535897932384626433....";
 echo "\nCalculated value of Pi when P is ".$p."\n";
 echo getPi($p); // 3.0418396189294
 
 $p = 100000000; // seed value 
 echo "\nCalculated value of Pi when P is ".$p."\n";
 echo getPi($p); // 3.1415926435893
 
 $p = 10000000000; // seed value 
 echo "\nCalculated value of Pi when P is ".$p."\n";
 echo getPi($p); // 3.141592643589326

 echo "\n\n";
